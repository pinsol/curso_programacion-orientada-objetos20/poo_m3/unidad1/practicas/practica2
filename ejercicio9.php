<!-- Ejercicio comentado -->
<html>
    <head>
            <title>Ejercicio 9</title>
    </head>
    <body>
        <p>
            Vamos a colocar en la pagina web los numeros del 1 al 10
            <?php
                for($c=1;$c<11;$c++)    //bucle for desde el contador en 1; hasta que sea menor de 11; haciendo q suma uno cada vez
                {
                    echo "<hr>$c";      // mostramos los resultados del bucle, es decir, del 1 al 10
                }
            ?>
            Vamos a mostrar los pesos de cada uno de los alumnos
            <?php
                $pesos = array(         //creacion de una array asociativa
                    "jaime" => 100,
                    "Jose" => 80,
                    "ana" => 75
                );
            // Queremos mostrar en una tabla los pesos de cada uno de los alumnos
            // junto con su nombre
            ?>
        </p>
        <table width="100%" border="1">     <!-- creando la tabla -->
            <tr>                            <!-- creacion de linea -->
                <?php
                        foreach($pesos as $indice=>$valor)  //recorre la array y añade en valor
                        {
                ?>                
                    <td>
                        <?php
                             echo $indice;                  //muestra los indices recorridos en el bucle foreach, es decir, los nombres
                        ?>
                    </td>
                <?php
                        }
                ?>
            </tr>
            <tr>
                <?php
                        foreach($pesos as $indice=>$valor)  
                        {
                            echo "<td>$valor</td>";        //muestra los valores recogidos de la array cada vez
                        }
                ?>
            </tr>
        </table>
    </body>
</html>